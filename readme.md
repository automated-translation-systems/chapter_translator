# Automated Translation System repo

Hope this repo finds you well.  This is mainly being created to be used by a book club that loves to read chinese books that are being written and posted before being completed.  
We are putting together a way to more quickly translate and post new chapters as they are created.

The goal of this system is to be able to:

1.  Go to a Website
2.  Find a chapter of interest
3.  Pull the original language for that chapter
4.  Translate it to a language of choice (english default)
5.  Output a text file with the new translation

## Extras

1.  Create a "new" google sheet and post data to that google sheet
2.  Append to a google sheet that already exists
3.  Append to the bottom of a text file with each chapter number shown
4.  Export to an epub format
5.  Export to a pdf
6.  Export to a webpage
7.  Send as an email
8.  Attache results to a discord channel using a discort bot and defined channel
